<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ArticleType;
use App\Entity\Article;
use App\Repository\ArticleRepository;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
        $article = $repo->findAll();
        return $this->render('blog/index.html.twig', [
        "article" => $article
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('blog/home.html.twig');
    }
    /**
     * @Route("/add-article", name="add-article")
     */
    public function addArticle(Request $request, ObjectManager $manager)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($article);
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog');
        }
        return $this->render("blog/add-article.html.twig", [
            "form" => $form->createView()
        ]);
    }
    /**
     * @Route("/see-article/{id}", name="see-article")
     */
    public function seeArticle(int $id, ArticleRepository $repo){
        $article=$repo->find($id);
        return $this->render('blog/see-article.html.twig',[
            "article" => $article
         ]);
    }
    /**
     * @Route("/remove/article/{article}", name="remove")
     */
    public function remove(Article $article, ObjectManager $manager){
        $manager->remove($article);
        $manager->flush();
        return $this->redirectToRoute('blog');
    }
     /**
     * @Route("/modify/{article}", name="modify")
     */
    public function modify(Request $request, Article $article, ObjectManager $manager)
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { 
            dump($article);
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog');
        }
        return $this->render("blog/add-article.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
