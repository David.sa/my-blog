USE blog;
CREATE TABLE article (
		id_article INT PRIMARY KEY AUTO_INCREMENT UNIQUE, 
        title_article VARCHAR(50) NOT NULL, 
        date_article DATE, 
        category_article VARCHAR(50) NOT NULL,
        img_article VARCHAR(150),
        content_article VARCHAR(10000),
        author_article VARCHAR(100)
        );
CREATE TABLE acceuil (
		id_acceuil INT PRIMARY KEY AUTO_INCREMENT UNIQUE, 
        title_acceuil VARCHAR(100) NOT NULL,
        date_acceuil DATE,
		img_acceuil VARCHAR(150),
        content_acceuil VARCHAR(10000)
        );
INSERT INTO article(
		title_article, 
        date_article, 
        category_article, 
        img_article, 
        content_article, 
        author_article
        ) 
VALUES(
		"test",
        NOW(),
        "Jeux Video",
        "https://engineeringtutorial.com/wp-content/uploads/2016/07/Transformer-Open-and-Short-Circuit-Tests.png",
        "ceci est un test pour verifier le fonctionnement du blog",
        "David.S");
INSERT INTO acceuil(
		title_acceuil,
        date_acceuil,
        img_acceuil,
        content_acceuil
        )
VALUES(
		"TEST de l acceuil",
        NOW(),
        "http://afcoaching.e-monsite.com/medias/images/test1.jpg?fx=r_550_550",
        "ceci est un test pour verifier la fonctionnalité de la page d'acceuil"
        );