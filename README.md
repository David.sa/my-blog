# Blog Simplon

## Présentation du projet

Le but de ce projet est d'utiliser les competences aquises avec **SYMFONY 4** durant les cours, et d'en developper de nouvelles grace a l'auto-formation. j'ai également utilisé **DOCTRINE** pour ce projet. 
Dans cet esprit j'ai entrepris un blog avec une vue plutot axé *administrateur* ou il est possible (pour l'admin) d'ajouter des articles, les modifiées, les supprimées (**CRUD**).

## Languages utilisées

* **HTML 5**
* **CSS 3**
* **MYSQL**
* **PHP 7**

## Interet

1. Mise en pratique des connaissances

2. utilisation et apprentissage de **SYMFONY 4**, **DOCTRINE**, **PHP 7** et **MYSQL**
 
## Modifications

**la premiere modification** éffectuée à été un reboot complet du projet, j'ai remis à plat mon code et pour un gain de temps j'ai décidé de tout recommencer avec **DOCTRINE**.

**les modifications a venir concerneront:**

* pouvoir changer la page d'acceuil via un formulaire.

* pouvoir afficher les titres dans leur intégralité tout en conservant leur dimension.

* "activé" la barre de recherche(Article Repository, Methode, search).

* voir comment améliorer la mise en page des articles.

* ajouter la possibilité de pouvoir commenter les articles (nouvelle Entity, relation One To Many).

## Exemple de code utilisé

Sélection d'une fonction de mon code (ArticleController) **PHP 7**

```
 /**
     * @Route("/add-article", name="add-article")
     */
    public function addArticle(Request $request, ObjectManager $manager)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($article);
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog');
        }
        return $this->render("blog/add-article.html.twig", [
            "form" => $form->createView()
        ]);
```
Et ici un exemple d'une de mes pages Template
```
        {% extends "base.html.twig" %}
        {% block body %}
        <div class="jumbotron" style="margin-top:10px;">
            <h1 class="display-3">{{article.title}}</h1>
            <p class="lead">{{article.date|date('d-m-Y')}} catégory : {{article.category}}</p>
            <img src="{{article.img}}" class="card-img-top" style="width:350px; height:200px;" alt="">
            <hr class="my-4">
            <p class="content" style="font-size:20px;">{{article.content}}</p>
            <p class="author" style="font-style:italic;">{{article.author}}</p>
            <p class="lead">
                <a href="{{path('remove',{article:article.id})}}" type="button" class="btn btn-danger">Remove</a>
                <a href="{{path('modify',{article:article.id})}}" type="button" class="btn btn-warning">Modify</a>
            </p>
        </div>
        {% endblock %}
    
```
## Diagramme Use Case

![20% center](./doc/UseCaseDiagram1.png)

## Base de données SQL

![20% center](./doc/BDD.png)

## wireframe

![20% center](./doc/wireframe-blog.png)

## Blog

voici la page d'acceuil de mon blog a partir de laquel on peux se deplacer sur la liste des articles via 2 boutons (celui de la nav-bar et le bouton "articles -->"):

![20% center](./doc/acceuil-blog.png)

la liste des articles que j'ai agrementé avec un petit badge, et un bouton "read" pour voir l'article en détaille:

![20% center](./doc/article-blog.png)

le formulaire pour ajouté ou modifié un article:

![20% center](./doc/form-blog.png)

et la vue détaillée d'un article que l'on peux supprimé ou modifié grace au boutons associé:

![20% center](./doc/see-article-blog.png)

## Difficultés

La création de la base de données n'as pas été trés difficile, le plus dur a été de comprendre le fonctionnement de **workbench**, en revanche j'ai eu de grosses difficultés a comprendre le fonctionnement de **PDO** dans **SYMFONY 4**. 

## Axes d'amélioration

Cela m'a poussé à basculer sur **DOCTRINE** qui m'as pas mal simplifié la vie et permis de faire un **CRUD** assez simplement.

## Ce que j'ai aimé faire

Ma partie préféré a été le front que j'ai réalisé avec **BOOTSWATCH**(pour le thème et la NAV-BAR) et **BOOTSTRAP**(pour l'acceuil et les articles).

## Pour lancer mon projet

1. Cloner le projet
2. Installer les dépendances avec composer install
3. Créer un fichier .env.local avec dedans DATABASE_URL=mysql://user:password@127.0.0.1:3306/db_name en remplaçant user, password et db_name par vos informations de connexion à mariadb
4. Faire un bin/console doctrine:migrations:migrate pour mettre la bdd dans le bon état (si jamais ça marche pas, tenter de faire un bin/console doctrine:schema:drop --force avant de refaire le migrate pour remettre à zéro la bdd)

## Copyright

©2018-2019 DavidSalvador All right reserved.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)